import { api } from './API_CONST';

interface Task {
  _id: string;
  name: string;
  content: string;
  completed: boolean;
  author: string;
  createdAt: Date;
}

async function getAllTasks() {
  return api.get('/tasks');
}

async function createTask(name: string) {
  return api.post('/tasks', { name: name });
}

async function updateCompleted(ID: string, completed: boolean) {
  return api.patch('/tasks/' + ID, { completed });
}

async function updateName(ID: string, name: string) {
  return api.patch('/tasks/' + ID, { name: name });
}

async function deleteTask(ID: string) {
  return api.delete('/tasks/' + ID);
}

export {
  getAllTasks,
  updateCompleted,
  Task,
  updateName,
  createTask,
  deleteTask,
};
