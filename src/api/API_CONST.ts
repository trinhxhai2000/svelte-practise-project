import axios from 'axios';

export const api = axios.create({
  baseURL: 'https://task-server-txhai12.herokuapp.com/api/v1/',
  timeout: 5000,
  withCredentials: true,
});
