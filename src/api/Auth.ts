import { api } from './API_CONST';
import { currentUser } from '../stores/CurrentUser';

async function login(username, password: string) {
  const result = await api.post('/login', {
    username,
    password,
  });

  // optimize leter, by return user id when login
  const user = (await getLoginUser()).data.user;
  currentUser.set(user);

  return result;
}

async function register(username, password: string) {
  return api.post('/register', {
    username,
    password,
  });
}

async function logout() {
  return api.post('/logout').then((res) => {
    currentUser.set(null);
    return res;
  });
}

async function getLoginUser() {
  return api.get('/getLoginUser');
}

export { login, register, getLoginUser, logout };
