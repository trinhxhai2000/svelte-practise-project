export interface Link {
  text: string;
  url: string;
  authMode: boolean;
}
export const Links: Link[] = [
  {
    text: 'Home',
    url: '/',
    authMode: true,
  },
  {
    text: 'Login',
    url: '/login',
    authMode: false,
  },
  {
    text: 'Register',
    url: '/register',
    authMode: false,
  },
  {
    text: 'About',
    url: '/about',
    authMode: false,
  },
];
