export interface CurrentUser {
  userID: string;
  username: string;
}
