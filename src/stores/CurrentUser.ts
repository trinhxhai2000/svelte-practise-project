import { writable } from 'svelte/store';
import { CurrentUser } from '../modals/CurrentUser';

const currentUser = writable<CurrentUser>(null);

export { currentUser };
