import { writable } from 'svelte/store';

const flash = writable<Flash>(null);

interface Flash {
  message: string;
  type: FlashType;
}

enum FlashType {
  ERROR = 'error',
  ACCEPT = 'accept',
  WARNING = 'warning',
}

export { flash, Flash, FlashType };
