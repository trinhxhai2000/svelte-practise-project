import { writable, derived, get } from 'svelte/store';
import {
  getAllTasks,
  deleteTask as apiDeleteTask,
  updateCompleted as apiUpdateCompletedTask,
  updateName as apiUpdateNameTask,
  createTask as apiCreateTask,
} from '../api/Task';
import { HTTPStatusCode } from '../utils/ApiStatusCode';

export const storeTasks = writable([], () => {
  setTasks();
  return () => {};
});

async function setTasks() {
  let tasks = await getAllTasks();
  console.log('store setTasks', tasks);
  storeTasks.set(tasks.data.tasks);
}

export async function createTask(name: string) {
  return apiCreateTask(name).then((result) => {
    if (result.status === HTTPStatusCode.Created) {
      const newTask = result.data.task;
      console.log('newTask', newTask);

      storeTasks.update((tasks) => [...tasks, newTask]);
    }
    return result;
  });
}

export async function updateCompletedTask(taskID: string, completed: boolean) {
  return apiUpdateCompletedTask(taskID, completed).then((result) => {
    if (result.status === HTTPStatusCode.OK) {
      storeTasks.update((tasks) => {
        return tasks.map((task) => {
          if (task._id === taskID) {
            task.completed = completed;
          }
          return task;
        });
      });
    }
    return result;
  });
}

export async function updateNameTask(taskID: string, name: string) {
  return apiUpdateNameTask(taskID, name).then((result) => {
    if (result.status === HTTPStatusCode.OK) {
      storeTasks.update((tasks) => {
        return tasks.map((task) => {
          if (task._id === taskID) {
            task.name = name;
          }
          return task;
        });
      });
    }
    return result;
  });
}

export async function deleteTask(taskID: string) {
  return apiDeleteTask(taskID).then((result) => {
    if (result.status === HTTPStatusCode.OK) {
      storeTasks.update((tasks) => tasks.filter((item) => item._id !== taskID));
    }
    return result;
  });
}

export const completedTasks = derived(storeTasks, ($tasks) => {
  return $tasks.filter((item) => item.completed === true);
});

export const uncompletedTasks = derived(storeTasks, ($tasks) => {
  return $tasks.filter((item) => item.completed === false);
});
